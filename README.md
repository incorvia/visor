# Visor.io

## Environment Setup

### Ruby
**Install**
ruby 2.0.0p247 (Install through rbenv or rvm)

**Notes**
bundle install to install necessary gems

### ENV
Environment is managed through foreman *.env* files.  Each environment will have its own *.env.{{ENVIRONMENT}}* (i.e. .env.test) file that can be run using foreman's -e flag.  A typical file have the content:

DATABASE_URL: "postgres://incorvia@localhost/visor_development?pool=5"

This will load ENV['DATABASE_URL'].

*Note that these .env files are ignored by git.  Also .env is the default, so you only need to specify the -e flag for test / other environments*

### Postgresql
**Install**
brew install postgresql

**Notes**
1. Make sure to setup .env file and .env.test files.  Note, these are ignored by .gitignore.
2. Run _foreman run rake db:refresh_
3. Setup test database with _foreman run rake db:test:refresh_
4. Generate a model diagram with: 'foreman run railroady -M | dot -Tsvg > docs/models.svg'

** Downloading Product Dump **

In order to download production dump you can do the following:

1. heroku plugins:install https://github.com/ddollar/heroku-pg-transfer
2. heroku pg:transfer -a visor-{{ staging or production}} --to {{ your database URL (get from your .env file) }}


### Front-End
Start the server with *foreman start* to run server, then visit: http://localhost:5000.  You may wish to alias this command locally to: fstart

*Note that this will load the environment within .env by default.*

### Admin
visit: http://localhost:5000/admin.

Login with username: *admin@example.com* & password: *password*.

### Tests
Start test server (guard) with *foreman run guard -e .env.test*.  You may wish to alias this command locally to: fguard

## Scoring
Scoring is a multi-step / multi-pass process.  This can probably be optimized but will suffice for now:

1. First for each spec, every spec\_value is found, compared, and scores are created. (Calculator::ValueToScore)
2. Second, for every product, spec\_group spec\_values are created based on product's scores & spec's weightings.
   If a required spec is missing a spec value such that we cannot create a spec, we should mark the product un-reviewable.
3. A final product rating is generated for every product.

Note:  Initially at least, every product will need to have every score, otherwise it will get a 0 for that score.  This means we will need to be vigilant about keeping scores up until we implement a system where we skip products that haven't gotten scored.  It also means that every scores is deemed to be an essential quality of that product and not having it gives it a 0 for that product.

## Generating Content
This documentation is meant to help a programmer/writer understand how Visor generates content.  The five major components in this process are:

1.  Templates
2.  Qualifiers
3.  Tags
4.  Product Data

### Templates
Visor uses liquid templates that uses tags and product data (inserted into templates via drops) to render the final version of content for a product review.  Templates use the ancestry gem to act as a tree.  This means that a template can have many templates allowing template nesting. This helps us determine when rendering a template which templates are eligible for inclusion when rendering.  

For example when rendering a review template, the review template may have n number of templates that belong to it.  Of these templates a number may refer to an intro paragraph and it will be from those templates that we will look when rendering that review.

Templates have names and these names should be fairly long and should indicate the purpose of the template:  "opening\_sentence\_that\_introduces\_product".  When a  template is attempting to include another template it will look up templates by name.  As mentioned before the purpose of Visor is to create variety, there may be 'n' number of templates that belong to the other template with this same name.  

Of these potential n number of templates that share a name, the renderer needs to determine which ones are eligible given the product in context.  From those that are eligible, one will be randomly selected.  This eligibility is determined via a method named passed into the template includer tag.  Writers will have a list of qualifiers which will execute on the product in context and return true or false.  If true they will be included in the random sampling.  It is these conditions that differentiate templates with the same name.  A condition will be a if/else statement.  

A category has a has_one relationship with templates which means that some templates will have a category_id and these templates are special in that they are the base template responsible for rendering content.

#### Qualifiers

A qualifier determines whether or not a template should be included in the template selection sampling.  It exists as a method name as an argument passed to the template includer tag and it will be evaluated at render time in the context of the product being reviewed to a true or false value.  An example of a qualifier method name might be "overall_rank".  Qualifiers may operate under the premise that products are divided into 8 different percentile groups:

   * rank 100 - 100 percentile
   * rank 95 - 99 percentile
   * rank 90 - 94 percentile
   * rank 75 - 89 percentile
   * rank 50 - 74 percentile
   * rank 25 - 49 percentile
   * rank 2 - 24 percentile
   * rank 1 - 1 percentile

Lets look at an example.  For the "opening\_sentence\_that\_introduces\_product", in the database there are 8 of these each with a different condition.  The first of the 8 evaluates to true if the product has the best overall ranking, the second of the eight evaluates to true if it is not the best but is in the top 4 percent overall ranking, etc.  Thus you can see it is important to have at least one template for each of the 8 different stratifications.  If we have more than 1 for a given level then they are randomly sampled.

Another way that products may be stratified is by how old they are.  In this case instead of stratifying on product ranking we stratify on how old it is.

   * Unreleased
   * greater than 0 days old less than 10 days old
   * greater than 10 days less than 60 days old
   * greater than 60 days less than 180 days old
   * greater than 180 days old less than 365 days old
   * greater than 365 days old less than 730 days old
   * greater than 730 days old
   
When a writer is adding a new type of qualifier, they will probably want to divide the qualifier up into stratifications and then they must ensure that there is a template that will evaluate to true for any given product for each of the stratification layers.  A coder will help to actually implement the qualifier.
   
Note again that it is important that every product must match exactly one and only one stratification.

Also note that not all snippets will have qualifiers. 

### Tags
Tags are simple liquid tags and are used for both including templates into other templates but also for displaying product information inside of a rendered snippet.  A full list of available tags can be found in liquids documentation.

### Product Information
Every review renders in the context of a product and qualifiers and tags use the product in context for determining outcomes.

### Uploading Photos
In order to prevent having to implement a uploader tool, we are uploading directly to S3.  To do this you need credentials.
Ask Kevin to give you credentials if you don't have them.  Once you have them get a tool like CyberDuck or ForkLift and
setup a Amazon S3 Bucket account.  Input your acces id and secret key and you should be able to see our buckets. 

  * You most likely should not need to touch the "development", "staging", or "production" buckets.
  * Before uploading make sure to crunch your image so it is web optimized.
  * There is a temp bucket.  Please upload your photos there.  Once uploaded, make sure to modify the permissions so that "others" or "anyone" can read it.
  * Most FTP programs that allow S3 Access allow you to right click on a photo and get the S3 URL, copy this.
  * Go to the appropriate backend, either your local admin, staging admin or production admin and create or modify an image.
  * Place the S3 url you have copied into the image_url field.
  * If creating a new image create a descriptive underscored name like "iphone_5s_3_colors"
  * Tags are meant to help find images.  This is a comma seperated list.
  * Gallery list are galleries you want this image to show up in.  This is a comma seperated list. The right sidebar gallery is called: "right_sidebar_1"
  * Locations are locations that you want the image to show up in. This is a comma seperated list. The most important image is "vanity", that will cause this picture to be used as the vanity.  Only one vanity should exist for a given product.
  * Once uploaded verify that the image works.
  * Use dragonfly directives for on-the-fly customizations.
  * Once uploaded, remove your photos from the temp bucket.
