require 'test_helper'

describe 'Filters' do

  class TestClass
    include Filters
  end

  let(:filters) { TestClass.new }

  describe 'ordinalize' do

    it 'ordinalizes and delimits' do
      filters.ordinalize(1500).must_equal('1,500th')
    end
  end

  describe 'delimit' do

    it 'delimits' do
      filters.delimit(1500).must_equal('1,500')
    end
  end

  describe 'round' do
    it 'rounds to the nearest significant digit' do
      filters.round(1.4).must_equal(1)
      filters.round(1.6).must_equal(2)
      filters.round(6).must_equal(6)
      filters.round(12).must_equal(10)
      filters.round(1001).must_equal(1000)
      filters.round(1501).must_equal(2000)
    end
  end
end
