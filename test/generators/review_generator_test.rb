require "test_helper"

describe ReviewGenerator do
  let(:product) { FactoryGirl.build(:product) }
  let(:category) { FactoryGirl.build(:category) }

  before :each do
    @product = FactoryGirl.create(:product)
    category = @product.category
    category.save
    FactoryGirl.create(:template, content: 'The {{product.name}} is amazing.', category_id: category.id)
    Product.any_instance.stubs(:top_product).returns(@product)

  end

  it 'generates reviews' do
    ReviewGenerator.run!
    @product.review.content.must_equal('The iPhone 5s is amazing.')
  end
end
