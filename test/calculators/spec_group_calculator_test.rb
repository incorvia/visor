require "test_helper"

describe SpecGroupCalculator do
  let(:category) { FactoryGirl.create(:category) }
  let(:spec_group) { SpecGroup.new(category_id: category.id) }
  let(:product) { FactoryGirl.create(:product, category_id: category.id) }
  let(:calculator) { SpecGroupCalculator.new(spec_group) }

  describe 'integration tests' do
    before :each do
      spec1 = FactoryGirl.create(:spec, weight: 4, include: true)
      spec2 = FactoryGirl.create(:spec, weight: 3, include: true)
      spec3 = FactoryGirl.create(:spec, weight: 2)

      spec_group.specs << spec1
      spec_group.specs << spec2
      spec_group.specs << spec3
      spec_group.save

      Score.create(
        product_id: product.id,
        score_value: 6,
        scoreable_type: 'Spec',
        scoreable_id: spec1.id,
        spec_value_id: 1
      )

      Score.create(
        product_id: product.id,
        score_value: 3,
        scoreable_type: 'Spec',
        scoreable_id: spec2.id,
        spec_value_id: 1
      )

      Score.create(
        product_id: product.id,
        score_value: 8,
        scoreable_type: 'Spec',
        scoreable_id: spec3.id,
        spec_value_id: 2
      )

      SpecGroupCalculator.run!

      scores = Score.all.to_a
      scores.sort_by { |x| x.score_value.to_f }.reverse
      @score = spec_group.scores.first
    end

    describe '#run!' do

      it 'calculates scores & rankings' do
        @score.score_value.to_f.must_equal(4.714285714285714)
        @score.rank.must_equal(1)
        @score.percentile.must_equal(100)
      end

      it 'updates scores' do
        actual = @score.score_value.to_f

        @score.score_value = 0.09
        @score.save

        SpecGroupCalculator.generate_scores!

        @score.reload
        @score.score_value.to_f.must_equal(actual)
      end
    end
  end
end
