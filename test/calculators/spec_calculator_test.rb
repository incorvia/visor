require "test_helper"

describe SpecCalculator do

  describe 'integration tests' do
    before :each do
      @spec = FactoryGirl.create(:spec)

      [1,2,3].each_with_index do |x, index|
        value = @spec.spec_values.new.tap do |spec|
          spec.product_id = index
          spec.spec_id = @spec.id
          spec.value = (x + 1) * 2
        end
        value.save
      end
      @values = @spec.spec_values

      SpecCalculator.run!

      values = @values.sort_by { |v| v.value.to_f }.reverse
      scores = values.map(&:score)
      @s1, @s2, @s3 = scores
    end

    it 'creates scores' do
      @s1.score_value.to_f.must_equal(100.0)
      @s1.rank.must_equal(1)
      @s1.percentile.must_equal(100)
      @s2.score_value.to_f.must_equal(75)
      @s2.rank.must_equal(2)
      @s2.percentile.must_equal(66)
      @s3.score_value.to_f.must_equal(50)
      @s3.rank.must_equal(3)
      @s3.percentile.must_equal(33)
    end

    it 'updates scores' do
      actual = @s1.score_value.to_f

      @s1.score_value = 0.09
      @s1.save

      SpecCalculator.run!

      @s1.reload
      @s1.score_value.to_f.must_equal(actual)
    end
  end
end
