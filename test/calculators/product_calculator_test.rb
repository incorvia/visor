require "test_helper"

describe ProductCalculator do
  let(:category) { FactoryGirl.create(:category) }
  let(:product) { FactoryGirl.create(:product, category_id: category.id) }

  describe 'integration tests' do

    before :each do
      sg1 = FactoryGirl.create(:spec_group, weight: 3, include: true)
      sg2 = FactoryGirl.create(:spec_group, weight: 2, include: true)

      category.spec_groups << sg1
      category.spec_groups << sg2
      category.save

      @score1 = Score.create(
        product_id: product.id,
        scoreable_id: sg1.id,
        scoreable_type: 'SpecGroup',
        score_value: 5
      )

      @score2 = Score.create(
        product_id: product.id,
        scoreable_id: sg2.id,
        scoreable_type: 'SpecGroup',
        score_value: 6
      )

      ProductCalculator.run!
    end

    it 'calculates the score' do
      score = product.product_score
      score.score_value.to_f.must_equal(5.4)
    end
  end

  describe '#generate_ranking!' do

    before(:each) do
      p1 = FactoryGirl.create(:product, category_id: category.id)
      p2 = FactoryGirl.create(:product, category_id: category.id)
      p3 = FactoryGirl.create(:product, category_id: category.id)

      @score1 = Score.create(
        product_id: p1.id,
        scoreable_id: p1.id,
        scoreable_type: 'Product',
        score_value: 5
      )

      @score2 = Score.create(
        product_id: p2.id,
        scoreable_id: p2.id,
        scoreable_type: 'Product',
        score_value: 5
      )

      @score3 = Score.create(
        product_id: p3.id,
        scoreable_id: p3.id,
        scoreable_type: 'Product',
        score_value: 6
      )

      ProductCalculator.generate_rankings!
    end

    it 'ranks appropriately' do
      @score1.reload
      @score1.rank.must_equal(2)
      @score1.percentile.must_equal(66)
      @score2.reload
      @score2.rank.must_equal(2)
      @score2.percentile.must_equal(66)
      @score3.reload
      @score3.rank.must_equal(1)
      @score3.percentile.must_equal(100)
    end
  end
end
