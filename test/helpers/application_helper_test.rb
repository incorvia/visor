require "test_helper"

describe ApplicationHelper do
  let(:helper) do
    class Helper; include ApplicationHelper; end
    Helper.new
  end

  describe 'title_tag' do

    it 'returns default title content' do
      helper.title_tag.must_equal("Visor")
    end

    it 'returns the content title when present' do
      content = OpenStruct.new(title: 'Best Product Review')
      helper.title_tag(content).must_equal("Best Product Review | Visor")
    end
  end

  describe 'description_tag' do

    it 'returns default title content' do
      helper.description_tag.must_equal("Visor was founded in 2014 to provide intelligent and always up-to-date product reviews that stay current as the market for products changes.")
    end

    it 'returns the content title when present' do
      content = OpenStruct.new(summary: 'Best Product Review')
      helper.description_tag(content).must_equal("Best Product Review")
    end
  end
end