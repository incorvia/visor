require "test_helper"

describe ProductDrop do
  let(:product) { FactoryGirl.create(:product) }

  describe '#product' do

    describe '#name' do

      it 'returns the product name' do
        drop = ProductDrop.new(product)
        drop.name.must_equal(product.name)
      end
    end

    describe '#brand_name' do

      it 'returns the products brand name' do
        product.stubs(:brand).returns(FactoryGirl.build(:brand))
        drop = ProductDrop.new(product)
        drop.brand_name.must_equal(product.brand.name)
      end
    end

    describe '#full_name' do

      it 'returns the products full_name' do
        product.stubs(:brand_name).returns('Apple')
        product.stubs(:name).returns('iPhone')
        drop = ProductDrop.new(product)
        drop.full_name.must_equal('Apple iPhone')
      end
    end

    describe '#better_products' do

      it 'returns the number of better products' do
        product.stubs(:rank).returns(5000)
        drop = ProductDrop.new(product)
        drop.better_products.must_equal(4999)
      end
    end

    describe '#rank' do

      it 'returns the number of better products' do
        product.stubs(:rank).returns(5000)
        drop = ProductDrop.new(product)
        drop.rank.must_equal(5000)
      end
    end
  end
end
