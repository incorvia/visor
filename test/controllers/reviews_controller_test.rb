require "test_helper"

describe ReviewsController do

  describe '#show' do

    before :each do
      @review = create(:review)
      @product = create(:product)
      @review.product = @product
      @review.save
    end

    it "succeeds with a slug" do
      get :show, id: @review.slug
      assert_response :success
      assigns[:review].must_equal(@review)
    end

    it "succeeds with an id" do
      get :show, id: @review.id
      assert_response :success
      assigns[:review].must_equal(@review)
    end

    it 'sets @product' do
      get :show, id: @review.id
      assigns[:product].must_equal(@product)
    end

    describe 'sidebar_gallery' do

      it 'sets @sidebar_gallery' do
        create(:image, gallery_list: 'bad')
        good = create(:image, gallery_list: 'right_sidebar_1')
        @product.images << good
        @product.save
        get :show, id: @review.id
        assigns[:sidebar_gallery].count.must_equal(1)
        assigns[:sidebar_gallery].first.id.must_equal(good.id)
      end

      it 'sets a default into @sidebar_gallery when no other images' do
        get :show, id: @review.id
        assigns[:sidebar_gallery].count.must_equal(1)
      end
    end

    describe 'related' do

      it 'sets related products' do
        related = create(:product)
        related.review = create(:review)
        related.save
        get :show, id: @review.id
        assigns[:related].count.must_equal(1)
        assigns[:related].first.id.must_equal(related.id)
      end
    end
  end
end
