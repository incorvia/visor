require "test_helper"

describe SpecValue do

  describe 'validations' do

    it 'validates uniquess by product_id and spec_id' do
      create(:spec_value, spec_id: 1, product_id: 2)
      sv = build(:spec_value, spec_id: 1, product_id: 2)
      sv.valid?.must_equal(false)
      sv.spec_id = 2
      sv.valid?.must_equal(true)
    end
  end
end
