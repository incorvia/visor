require "test_helper"

describe AdminUser do

  describe "validations" do
    let(:admin_user) { AdminUser.new }

    it "requires an email" do
      admin_user.valid?
      admin_user.errors.messages[:email].count.must_be :>, 0
    end

    it "requires a password" do
      admin_user.valid?
      admin_user.errors.messages[:password].count.must_be :>, 0
    end
  end
end
