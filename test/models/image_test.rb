require "test_helper"

describe Image do
  let(:image) { Image.new }

  it "requires a name field" do
    image.name = nil
    image.valid?
    image.errors[:name].count.must_be :>, 0
  end
end
