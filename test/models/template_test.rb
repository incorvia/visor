require "test_helper"

describe Template do

  describe 'validations' do

    before do
      @template = Template.new
    end

    it "must be valid" do
      @template.valid?.must_equal true
    end
  end

  describe '#underscore_name' do

    it 'underscores name before save' do
      template = build(:template)
      template.name = "This is my name!@#"
      template.save
      template.reload
      template.name.must_equal("this_is_my_name")
    end
  end

  describe '#render' do

    it 'renders the review' do
      template = create(:template, content: 'Hello World')
      template.render.must_equal('Hello World')
    end
  end

  describe '#children_by_name' do

    it 'returns children by name' do
      template = create(:template)
      t1 = create(:template, name: 'test', parent_id: template.id)
      create(:template, name: 'test2', parent_id: template.id)
      template.save
      templates = template.children_by_name('test').to_a
      templates.must_include(t1)
      templates.count.must_equal(1)
    end
  end

  describe '#child_by_rules' do

    before :each do
      @product = create(:product)
      @template = create(:template)
      @template.stubs(:children_by_name).returns(['test'])
      @product.stubs(:select_templates_by_rules).returns(['test2'])
    end

    it 'returns a template' do
      @template.child_by_rules('test', @product).must_equal('test2')
    end
  end
end
