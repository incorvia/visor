require "test_helper"

describe Product do
  let(:product) { build(:product) }
  let(:category) { build(:category) }

  describe 'validations' do
    let(:product) { Product.new }

    it "requires a name field" do
      product.name = nil
      product.valid?
      product.errors[:name].count.must_be :>, 0
    end

    it "requires a brand field" do
      product.brand_id = nil
      product.valid?
      product.errors[:brand_id].count.must_be :>, 0
    end
  end

  describe '#gallery' do

    before :each do
      @review = create(:review)
      @product = create(:product)
      @review.product = @product
      @review.save
    end

    describe 'images in gallery' do

      before :each do
        create(:image, gallery_list: 'bad')
        @good = create(:image, gallery_list: 'good')
        @key = create(:image, gallery_list: 'good', tag_list: 'gallery_key')
        @product.images << @good
        @product.images << @key
        @product.save
      end

      it 'returns images' do
        images = @product.gallery('good')
        images.count.must_equal(2)
      end

      it 'places gallery_key tagged photo first' do
        images = @product.gallery('good')
        images.first.id.must_equal(@key.id)
        images.count.must_equal(2)
      end
    end

    describe 'no images in gallery' do

      before :each do
        create(:image, gallery_list: 'bad')
      end

      describe 'return_default false' do

        it 'returns an empty array when return_default is false' do
          images = @product.gallery('good')
          images.count.must_equal(0)
        end
      end

      describe 'return_default true' do

        it 'returns a default image' do
          images = @product.gallery('good', true)
          images.count.must_equal(1)
          images.first.name.must_equal('Default Product Image')
        end
      end
    end
  end

  describe '#group_scores' do

    it 'returns only group scores' do
      product.save
      s1 = create(:score, scoreable_type: 'Spec', product_id: product.id)
      s2 = create(:score, scoreable_type: 'Product', product_id: product.id, scoreable_id: product.id)
      s3 = create(:score, scoreable_type: 'SpecGroup', product_id: product.id)
      product.scores << s1
      product.scores << s2
      product.scores << s3
      product.save
      scores = product.group_scores
      scores.count.must_equal(1)
      scores.first.id.must_equal(s3.id)
    end
  end

  describe '#category_specs' do
    let(:category) { create(:category) }
    let(:spec_group) { create(:spec_group) }
    let(:spec) { create(:spec) }
    let(:bad_spec) { create(:spec, name: 'bad') }

    before :each do
      product.category = category
      category.spec_groups << spec_group
      spec_group.specs << spec
      product.save
    end

    it 'returns spec groups of my category' do
      product.category_specs.count.must_equal(1)
      product.category_specs.must_include(spec)
    end
  end

  describe '#to_score' do
    let(:category) { create(:category) }
    let(:spec_group) { create(:spec_group, include: true, weight: 1, category_id: category.id) }
    let(:spec_group2) { create(:spec_group, include: true, weight: 2, category_id: category.id) }

    before :each do
      category.spec_groups << spec_group
      category.spec_groups << spec_group2
      category.save
      @product = create(:product, category_id: category.id)

      Score.create(
        product_id: @product.id,
        score_value: 5,
        scoreable_type: 'SpecGroup',
        scoreable_id: spec_group.id
      )

      Score.create(
        product_id: @product.id,
        score_value: 8,
        scoreable_type: 'SpecGroup',
        scoreable_id: spec_group2.id
      )
    end

    it 'returns tuples for spec groups' do
      @product.to_score.must_equal(7)
    end
  end

  describe '#render_review' do

    before :each do
      top = build(:product, name: 'Bad Product')
      product.save
      product.category.save
      product.stubs(:top_product).returns(top)
      create(:template, content: 'The {{product.name}} is better than the {{ top_product.name }}.', category_id: product.category.id)
      product.stubs(:top_product).returns(top)
    end

    it 'renders the review' do
      product.render_review.must_equal('The iPhone 5s is better than the Bad Product.')
    end
  end

  describe '#review_or_build' do

    it 'returns the review if it exists' do
      product.stubs(:review).returns('review')
      product.review_or_build.must_equal('review')
    end

    it 'returns a new Content::Review if no review' do
      product.save
      product.stubs(:review).returns(nil)
      review = product.review_or_build
      review.must_be_instance_of(Content::Review)
      review.product_id.must_equal(product.id)
    end
  end

  describe '#select_templates_by_rules' do

    before :each do
      @p = create(:product)
      rule = create(:rule, rule_method: 'test')
      @template = create(:template, name: 'temp', rule_id: rule.id)
      @templates = []
      @templates << @template
    end

    it 'selects templates whose rule evals to true' do
      Rule.stubs('test').returns(true)
      templates = @p.select_templates_by_rules(@templates)
      templates.count.must_equal(1)
    end

    it 'selects templates whose rule id is nil' do
      @template.update_attribute(:rule_id, nil)
      templates = @p.select_templates_by_rules(@templates)
      templates.count.must_equal(1)
    end

    it 'ignores templates whose rule evals to false' do
      Rule.stubs('test').returns(false)
      templates = @p.select_templates_by_rules(@templates)
      templates.count.must_equal(0)
    end
  end

  describe '#set_amazon_price' do

    it 'sets the amazon price when last lookup is old' do
      product.amazon_price = nil
      Amazon.any_instance.stubs(:price_lookup).returns(500)
      product.amazon_price_updated_at = 50.hours.ago
      product.set_amazon_price
      product.amazon_price.must_equal(500)
      product.amazon_price_updated_at.must_be :>, Time.now - 10.minutes
    end

    it 'sets the amazon price when recently updated' do
      product.amazon_price = 100
      Amazon.any_instance.stubs(:price_lookup).returns(500)
      product.amazon_price_updated_at = 40.hours.ago
      product.set_amazon_price
      product.amazon_price.must_equal(100)
    end
  end

  describe '#vanity' do

    before :each do
      @review = create(:review)
      @product = create(:product)
      @review.product = @product
      @review.save
    end

    describe 'images in location' do

      before :each do
        create(:image, location_list: 'bad')
        @good = create(:image, location_list: 'vanity')
        @product.images << @good
        @product.save
      end

      it 'returns the vanity' do
        image = @product.vanity
        image.id.must_equal(@good.id)
      end
    end
  end

  describe '#missing_properties' do

    before :each do
      category = create(:category)
      @product = create(:product, category_id: category.id)
      @property1 = create(:property, name: 'test1')
      @property2 = create(:property, name: 'test2')
      category.properties << @property1
      category.properties << @property2
      @product.product_properties.create(property_id: @property2.id, value: 1)
      category.save
    end

    it 'returns missing properties' do
      properties = @product.missing_properties
      properties.count.must_equal(1)
      properties.first.id.must_equal(@property1.id)
    end
  end

  describe '#spec_value' do

    it 'returns the value by name' do
      product.save
      spec = create(:spec, name: 'test')
      create(:spec_value, spec_id: spec.id, product_id: product.id, value: 5)
      product.stubs(:specs).returns([spec])
      product.spec_value('test').value.to_f.must_equal(5)
    end
  end

  describe '#spec_score' do

    it 'returns the score by name' do
      product.save
      spec = create(:spec, name: 'test')
      create(:score, scoreable_id: spec.id, scoreable_type: 'Spec', product_id: product.id, score_value: 5)
      product.stubs(:specs).returns([spec])
      product.spec_score('test').score_value.to_f.must_equal(5)
    end
  end

  describe '#spec_group_score' do

    it 'returns the score by name' do
      product.save
      group = create(:spec_group, name: 'test')
      create(:score, scoreable_id: group.id, scoreable_type: 'SpecGroup', product_id: product.id, score_value: 5)
      product.stubs(:spec_groups).returns([group])
      product.spec_group_score('test').score_value.to_f.must_equal(5)
    end
  end

  describe 'property_value' do

    it 'returns the value for the requested property' do
      product.save
      property = create(:property, name: 'test')
      create(:product_property, property_id: property.id, value: '5', product_id: product.id)
      product.property_value('test').must_equal('5')
    end
  end
end
