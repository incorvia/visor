require "test_helper"

describe Category do
  let(:category) { Category.new }

  describe 'validations' do

    it "must be valid" do
      category.valid?.must_equal true
    end
  end

  describe '#top_product' do

    it 'returns the top rated product' do
      p = create(:product, rank: 1)
      create(:product, rank: 2)
      category.stubs(:products).returns(Product.all)
      category.top_product.must_equal(p)
    end
  end
end
