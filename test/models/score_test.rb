require "test_helper"

describe Score do

  let(:score) { build(:score) }

  describe 'validations' do

    it "must be valid" do
      score.valid?.must_equal true
    end

    it 'must have a spec_value_id if type spec' do
      score.scoreable_type = 'Spec'
      score.spec_value_id = nil
      score.valid?.must_equal false
      score.spec_value_id = 1
      score.valid?.must_equal true
    end
  end

  describe '#set_rank_on_product' do

    describe 'product' do

      it 'sets the rank on the product' do
        p = create(:product)

        create(:score,
          rank: 1,
          percentile: 10,
          scoreable_type: 'Product',
          scoreable_id: p.id,
          product_id: p.id
        )

        p.reload
        p.rank.must_equal(1)
        p.percentile.must_equal(10)
      end
    end

    describe 'not product' do

      it 'sets the rank on the product' do
        p = create(:product, rank: nil, percentile: nil)

        create(:score,
          rank: 1,
          percentile: 10,
          scoreable_type: 'SpecGroup',
          scoreable_id: p.id,
          product_id: p.id,
          score_value: 5
        )

        p.reload
        p.rank.must_be_nil
        p.percentile.must_be_nil
        p.score_value.to_i.must_equal(5)
      end
    end
  end
end
