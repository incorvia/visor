require "test_helper"

describe Rule do
  before do
    @rule = Rule.new
  end

  it "must be valid" do
    @rule.valid?.must_equal true
  end

  describe '.product_percentile' do
    before :each do
      @product = create(:product)
      @product.stubs(:percentile).returns(100)
    end

    it 'returns true if product rank is within args' do
      Rule.product_percentile(@product, [90,100]).must_equal(true)
    end

    it 'returns false if product rank is outside of args' do
      Rule.product_percentile(@product, [20,50]).must_equal(false)
    end
  end

  describe '.spec_value_in_range' do
    before :each do
      @product = create(:product)
      @product.stubs(:spec_value).returns(build(:spec_value, value: 1))
    end

    it 'returns true if rating is within args' do
      Rule.spec_value_in_range(@product, ['Amazon User Rating', '0','2.5']).must_equal(true)
    end

    it 'returns false if rating is outsisde of args' do
      Rule.spec_value_in_range(@product, ['Amazon User Rating','2.5','5.0']).must_equal(false)
    end
  end

  describe '.spec_percentile_in_range' do
    before :each do
      @product = create(:product)
      @product.stubs(:spec_score).returns(create(:score, score_value: 1, product_id: 1))
    end

    it 'returns true if rating is within args' do
      Rule.spec_percentile_in_range(@product, ['Pixels Per Inch', 0,2.5]).must_equal(true)
    end

    it 'returns false if rating is outsisde of args' do
      Rule.spec_percentile_in_range(@product, ['Pixels Per Inch', 2.5,5.0]).must_equal(false)
    end
  end

  describe '.spec_percentile_in_range' do
    before :each do
      @product = create(:product)
      @product.stubs(:spec_group_score).returns(build(:score, score_value: 1, product_id: 1))
    end

    it 'returns true if rating is within args' do
      Rule.spec_group_percentile_in_range(@product, ['Pixels Per Inch', '0','2.5']).must_equal(true)
    end

    it 'returns false if rating is outsisde of args' do
      Rule.spec_group_percentile_in_range(@product, ['Pixels Per Inch', '2.5','5.0']).must_equal(false)
    end
  end

  describe '.match_property_value' do

    before :each do
      @product = create(:product)
      property = create(:property, name: 'display_type')
      @product.product_properties.create(property_id: property.id, value: 'test')
    end

    it 'returns true if the property value matches the arg' do
      Rule.match_property_value(@product, ['display_type', 'test']).must_equal(true)
    end

    it 'returns false if the property value does not match the arg' do
      Rule.match_property_value(@product, ['display_type', 'bad']).must_equal(false)
    end
  end
end
