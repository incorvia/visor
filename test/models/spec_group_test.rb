require "test_helper"

describe SpecGroup do
  let(:spec_group) { SpecGroup.new }

  describe "validations" do

    it "must be valid" do
      spec_group.valid?.must_equal true
    end
  end

  describe '#included_specs' do
    before :each do
      @bad = create(:spec)
      @good = create(:spec, include: true)
    end

    it 'returns included specs' do
      spec_group.specs << @good
      spec_group.specs << @bad
      spec_group.save
      spec_group.included_specs.count.must_equal(1)
      spec_group.included_specs.must_include(@good)
    end
  end
end
