require "test_helper"

describe Property do
  let(:property) { build(:property) }

  it "must be valid" do
    property.valid?.must_equal true
  end

  it 'must require a name' do
    property.name = nil
    property.valid?.must_equal false
  end
end
