require "test_helper"

describe Content do
  let(:content) { build(:content) }

  it "must be valid" do
    content.valid?.must_equal true
  end

  describe '#update_summary' do

    it 'sets the summary' do
      content.summary = nil
      content.content = "<p>test</p><p>test 2</p>"
      content.save
      content.reload
      content.summary.must_equal('test')
    end

    it 'does not set the summary if custom_summary is set' do
      content.summary = "existing"
      content.content = "<p>test</p><p>test 2</p>"
      content.custom_summary = true
      content.save
      content.reload
      content.summary.must_equal('existing')
    end
  end
end
