require "test_helper"

describe Spec do

  describe 'validations' do
    let(:spec) { Spec.new }

    it "requires a name field" do
      spec.valid?
      spec.errors[:name].count.must_be :>, 0
    end

    it 'validates uniqueness of name by spec_group_id' do
      create(:spec, name: 'test', spec_group_id: 1)
      spec = build(:spec, name: 'test', spec_group_id: 1)
      spec.valid?.must_equal(false)
      spec.spec_group_id = 2
      spec.valid?.must_equal(true)
    end
  end
end
