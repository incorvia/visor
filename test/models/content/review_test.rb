require 'test_helper'

describe Content::Review do

  describe '#default_title' do

    it 'creates a default title' do
      Content::Review.default_title('Camera').
        must_equal('Camera Review')
    end
  end
end
