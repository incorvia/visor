# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :review, class: Content::Review do
    title "My Review"
    content "My review."
    type "Content::Review"
    product_id 1
  end
end
