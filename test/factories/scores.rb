# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :score do
    rank 1
    percentile 1
    scoreable_type "MyString"
    scoreable_id 1
    spec_value_id 1
    score_value 5
  end
end
