# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :content do
    title "MyString"
    content "MyText"
    type ""
    product { build(:product) }
  end
end
