FactoryGirl.define do
  factory :category do
    name "Cell Phones"
    active_months 24
  end
end

