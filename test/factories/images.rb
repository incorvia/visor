# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :image do
    name 'MyImage'
    product_id 1
    description "MyText"
  end
end
