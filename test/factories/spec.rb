FactoryGirl.define do
  factory :spec do
    sequence(:name) {|n| "Talk Time #{n}" }
  end
end
