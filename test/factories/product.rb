FactoryGirl.define do
  factory :product do
    name "iPhone 5s"
    category { build(:category) }
    brand_id 1
    score_value 5
    percentile 90
    rank 5

    factory :product_with_values do
      ignore do
        value_count 2
      end

      after(:create) do |product, evaluator|
        FactoryGirl.create_list(:spec_value, evaluator.value_count, product: product)
      end
    end
  end
end

