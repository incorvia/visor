# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_property do
    property_id 1
    product_id 1
    value "MyString"
  end
end
