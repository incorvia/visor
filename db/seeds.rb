# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# AdminUsers
AdminUser.create(email: 'admin@example.com', password: 'password')

# Categories
c1 = Category.create(name: 'Cell Phones', active_months: 24)

# Spec Groups
sg1 = SpecGroup.create(name: 'Battery Life', category_id: c1.id, include: true)
sg2 = SpecGroup.create(name: 'Screen', category_id: c1.id, include: true)

# Specs
s1 = Spec.create(name: 'Talk Time', spec_group_id: sg1.id, weight: 2, include: true)
s2 = Spec.create(name: 'Standby Time', spec_group_id: sg1.id, weight: 1, include: true)
s3 = Spec.create(name: 'Pixels', spec_group_id: sg2.id, weight: 1, include: true)

# Brands
b1 = Brand.create(name: 'Apple')
b2 = Brand.create(name: 'Samsung')

# Products
p1 = Product.create(name: 'iPhone 5s', category_id: c1.id, asin: 'B00F3J4E5U', brand_id: b1.id)
p2 = Product.create(name: 'Galaxy S4', category_id: c1.id, asin: 'B00CRNW3ZI', brand_id: b2.id)

# p1 Spec Values
p1.spec_values.create(value: 10, spec_id: s1.id)
p1.spec_values.create(value: 250, spec_id: s2.id)
p1.spec_values.create(value: 727040, spec_id: s3.id)

# p2 Spec Values
p2.spec_values.create(value: 17, spec_id: s1.id)
p2.spec_values.create(value: 320, spec_id: s2.id)
p2.spec_values.create(value: 2073600, spec_id: s3.id)

# Rule
r1 = Rule.create(name: 'Product Percentile', rule_method: 'product_percentile')

# Template1
review_temp = """
  {% vinclude intro_sentence %}
"""

t1 = Template.create(
  name: 'camera_review_alpha_version',
  content: review_temp,
  category_id: c1.id
)

# Template 2
best_temp = """
  The {{product.name}} is the best phone on the market today.
"""

t2 = Template.create(
  name: 'intro_sentence',
  content: best_temp,
  parent_id: t1.id,
  rule_parameters: "100,100",
  rule_id: r1.id
)

# Template 3
mid_temp = """
  The {{product.name}} is a run of the mill phone.
"""

t2 = Template.create(
  name: 'intro_sentence',
  content: mid_temp,
  parent_id: t1.id,
  rule_parameters: "0,99",
  rule_id: r1.id
)
