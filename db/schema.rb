# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131228133851) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "brands", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "brands", ["name"], name: "index_brands_on_name", using: :btree

  create_table "categories", force: true do |t|
    t.string   "name",          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "active_months", null: false
  end

  create_table "categories_properties", force: true do |t|
    t.integer "category_id", null: false
    t.integer "property_id", null: false
  end

  add_index "categories_properties", ["category_id", "property_id"], name: "index_categories_properties_on_category_id_and_property_id", unique: true, using: :btree
  add_index "categories_properties", ["category_id"], name: "index_categories_properties_on_category_id", using: :btree
  add_index "categories_properties", ["property_id"], name: "index_categories_properties_on_property_id", using: :btree

  create_table "contents", force: true do |t|
    t.string   "title",                          null: false
    t.text     "content",                        null: false
    t.string   "type",                           null: false
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug",                           null: false
    t.text     "summary"
    t.boolean  "custom_summary", default: false
  end

  add_index "contents", ["product_id", "type"], name: "index_contents_on_product_id_and_type", unique: true, using: :btree
  add_index "contents", ["product_id"], name: "index_contents_on_product_id", using: :btree
  add_index "contents", ["slug"], name: "index_contents_on_slug", unique: true, using: :btree
  add_index "contents", ["type"], name: "index_contents_on_type", using: :btree

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "images", force: true do |t|
    t.string   "name",        null: false
    t.integer  "product_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_uid"
  end

  add_index "images", ["product_id"], name: "index_images_on_product_id", using: :btree

  create_table "product_properties", force: true do |t|
    t.integer  "property_id", null: false
    t.integer  "product_id",  null: false
    t.string   "value",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_properties", ["product_id", "property_id"], name: "index_product_properties_on_product_id_and_property_id", using: :btree
  add_index "product_properties", ["product_id"], name: "index_product_properties_on_product_id", using: :btree
  add_index "product_properties", ["property_id"], name: "index_product_properties_on_property_id", using: :btree

  create_table "products", force: true do |t|
    t.string   "name",                    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_id",             null: false
    t.string   "asin"
    t.integer  "brand_id",                null: false
    t.integer  "amazon_price"
    t.datetime "amazon_price_updated_at"
    t.integer  "rank"
    t.integer  "percentile"
    t.decimal  "score_value"
  end

  add_index "products", ["brand_id"], name: "index_products_on_brand_id", using: :btree
  add_index "products", ["category_id"], name: "index_products_on_category_id", using: :btree
  add_index "products", ["percentile"], name: "index_products_on_percentile", using: :btree
  add_index "products", ["rank"], name: "index_products_on_rank", using: :btree

  create_table "properties", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "properties", ["name"], name: "index_properties_on_name", unique: true, using: :btree

  create_table "rules", force: true do |t|
    t.string   "name",        null: false
    t.string   "rule_method", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rules", ["rule_method"], name: "index_rules_on_rule_method", using: :btree

  create_table "scores", force: true do |t|
    t.decimal  "score_value",    null: false
    t.integer  "rank"
    t.integer  "percentile"
    t.string   "scoreable_type", null: false
    t.integer  "scoreable_id",   null: false
    t.integer  "spec_value_id"
    t.integer  "product_id",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "scores", ["product_id", "scoreable_type", "scoreable_id"], name: "index_scores_on_product_id_and_scoreable_type_and_scoreable_id", unique: true, using: :btree
  add_index "scores", ["rank"], name: "index_scores_on_rank", using: :btree
  add_index "scores", ["scoreable_id"], name: "index_scores_on_scoreable_id", using: :btree
  add_index "scores", ["scoreable_type"], name: "index_scores_on_scoreable_type", using: :btree
  add_index "scores", ["spec_value_id"], name: "index_scores_on_spec_value_id", using: :btree

  create_table "spec_groups", force: true do |t|
    t.string   "name"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "include"
    t.integer  "weight"
  end

  add_index "spec_groups", ["category_id"], name: "index_spec_groups_on_category_id", using: :btree
  add_index "spec_groups", ["include"], name: "index_spec_groups_on_include", using: :btree

  create_table "spec_values", force: true do |t|
    t.integer  "product_id", null: false
    t.integer  "spec_id",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "value",      null: false
  end

  add_index "spec_values", ["product_id"], name: "index_spec_values_on_product_id", using: :btree
  add_index "spec_values", ["spec_id", "product_id"], name: "index_spec_values_on_spec_id_and_product_id", unique: true, using: :btree

  create_table "specs", force: true do |t|
    t.string   "name",          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "spec_group_id"
    t.boolean  "include"
    t.integer  "weight"
    t.text     "notes"
  end

  add_index "specs", ["include"], name: "index_specs_on_include", using: :btree
  add_index "specs", ["spec_group_id"], name: "index_specs_on_spec_group_id", using: :btree

  create_table "taggings", force: true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: true do |t|
    t.string "name"
  end

  create_table "templates", force: true do |t|
    t.string   "name",            null: false
    t.text     "content",         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "ancestry"
    t.integer  "category_id"
    t.integer  "rule_id"
    t.string   "rule_parameters"
  end

  add_index "templates", ["ancestry"], name: "index_templates_on_ancestry", using: :btree
  add_index "templates", ["category_id"], name: "index_templates_on_category_id", using: :btree
  add_index "templates", ["content"], name: "index_templates_on_content", using: :btree
  add_index "templates", ["name", "ancestry"], name: "index_templates_on_name_and_ancestry", using: :btree
  add_index "templates", ["name"], name: "index_templates_on_name", using: :btree
  add_index "templates", ["rule_id"], name: "index_templates_on_rule_id", using: :btree

end
