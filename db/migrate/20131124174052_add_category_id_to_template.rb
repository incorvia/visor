class AddCategoryIdToTemplate < ActiveRecord::Migration
  def change
    add_column :templates, :category_id, :integer
    add_index :templates, :category_id
  end
end
