class AddWeightToSpecGroup < ActiveRecord::Migration
  def change
    add_column :spec_groups, :weight, :integer
  end
end
