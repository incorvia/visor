class AddActiveMonthsToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :active_months, :integer, null: false
  end
end
