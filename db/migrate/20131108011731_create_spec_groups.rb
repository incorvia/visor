class CreateSpecGroups < ActiveRecord::Migration
  def change
    create_table :spec_groups do |t|
      t.string :name
      t.integer :category_id

      t.timestamps
    end
    add_index :spec_groups, :category_id
  end
end
