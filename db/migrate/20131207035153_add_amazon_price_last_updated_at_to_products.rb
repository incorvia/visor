class AddAmazonPriceLastUpdatedAtToProducts < ActiveRecord::Migration
  def change
    add_column :products, :amazon_price_updated_at, :datetime
  end
end
