class AddCategoryIdToProduct < ActiveRecord::Migration
  def change
    add_column :products, :category_id, :integer, null: false
    add_index :products, :category_id
  end
end
