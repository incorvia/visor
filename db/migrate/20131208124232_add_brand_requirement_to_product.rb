class AddBrandRequirementToProduct < ActiveRecord::Migration
  def change
    change_column :products, :brand_id, :integer, null: false
  end
end
