class AddSpecIdAndProductIdIndexToSpecValue < ActiveRecord::Migration
  def change
    add_index :spec_values, [:spec_id, :product_id], unique: true
  end
end
