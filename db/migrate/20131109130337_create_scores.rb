class CreateScores < ActiveRecord::Migration
  def change
    create_table :scores do |t|
      t.decimal :score_value, null: false
      t.integer :rank
      t.integer :percentile
      t.string :scoreable_type, null: false
      t.integer :scoreable_id, null: false
      t.integer :spec_value_id
      t.integer :product_id, null: false

      t.timestamps
    end
    add_index :scores, :rank
    add_index :scores, :scoreable_type
    add_index :scores, :scoreable_id
    add_index :scores, :spec_value_id
  end
end
