class AddAmazonPriceToProduct < ActiveRecord::Migration
  def change
    add_column :products, :amazon_price, :integer
  end
end
