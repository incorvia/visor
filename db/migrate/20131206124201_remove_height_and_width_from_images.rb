class RemoveHeightAndWidthFromImages < ActiveRecord::Migration
  def change
    remove_column :images, :height
    remove_column :images, :width
  end
end
