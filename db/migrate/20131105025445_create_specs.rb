class CreateSpecs < ActiveRecord::Migration
  def change
    create_table :specs do |t|
      t.string :name, null: false

      t.timestamps
    end
  end
end
