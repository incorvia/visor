class AddIncludeToSpec < ActiveRecord::Migration
  def change
    add_column :specs, :include, :boolean
    add_index :specs, :include
  end
end
