class AddIncludeToSpecGroup < ActiveRecord::Migration
  def change
    add_column :spec_groups, :include, :boolean
    add_index :spec_groups, :include
  end
end
