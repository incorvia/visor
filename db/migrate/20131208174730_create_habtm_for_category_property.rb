class CreateHabtmForCategoryProperty < ActiveRecord::Migration
  def change
    create_table :categories_properties do |t|
      t.integer :category_id, null: false
      t.integer :property_id, null: false
    end

    add_index :categories_properties, :category_id
    add_index :categories_properties, :property_id
    add_index :categories_properties, [:category_id, :property_id], unique: true
  end
end
