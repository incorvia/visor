class ModifyIndexOnProductProperty < ActiveRecord::Migration
  def change
    remove_index :product_properties, column: [:product_id, :product_id]
    add_index :product_properties, [:product_id, :property_id]
  end
end
