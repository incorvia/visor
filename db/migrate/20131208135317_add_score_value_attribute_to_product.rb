class AddScoreValueAttributeToProduct < ActiveRecord::Migration
  def change
    add_column :products, :score_value, :decimal
  end
end
