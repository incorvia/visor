class AddRuleParametersToTemplate < ActiveRecord::Migration
  def change
    add_column :templates, :rule_parameters, :string
  end
end
