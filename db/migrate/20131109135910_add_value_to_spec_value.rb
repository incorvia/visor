class AddValueToSpecValue < ActiveRecord::Migration
  def change
    add_column :spec_values, :value, :decimal, null: false
  end
end
