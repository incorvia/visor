class AddUniqueIndexToScore < ActiveRecord::Migration
  def change
    add_index :scores, [:product_id, :scoreable_type, :scoreable_id], unique: true
  end
end
