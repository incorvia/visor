class CreateTemplates < ActiveRecord::Migration
  def change
    create_table :templates do |t|
      t.string :name, null: false
      t.text :content, null: false

      t.timestamps
    end
    add_index :templates, :name
    add_index :templates, :content
  end
end
