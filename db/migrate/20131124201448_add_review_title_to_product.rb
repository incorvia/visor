class AddReviewTitleToProduct < ActiveRecord::Migration
  def change
    add_column :products, :review_title, :string
  end
end
