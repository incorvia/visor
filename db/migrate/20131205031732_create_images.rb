class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :name, null: false
      t.integer :product_id
      t.string :url, null: false
      t.integer :width
      t.integer :height
      t.text :description

      t.timestamps
    end
    add_index :images, :product_id
  end
end
