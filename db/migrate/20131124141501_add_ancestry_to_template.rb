class AddAncestryToTemplate < ActiveRecord::Migration
  def change
    add_column :templates, :ancestry, :string
    add_index :templates, :ancestry
    add_index :templates, [:name, :ancestry]
  end
end
