class RemoveReviewTitleFromProduct < ActiveRecord::Migration
  def change
    remove_column :products, :review_title
  end
end
