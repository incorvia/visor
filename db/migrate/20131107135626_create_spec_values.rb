class CreateSpecValues < ActiveRecord::Migration
  def change
    create_table :spec_values do |t|
      t.integer :product_id, null: false
      t.integer :spec_id, null: false

      t.timestamps
    end
    add_index :spec_values, :product_id
  end
end
