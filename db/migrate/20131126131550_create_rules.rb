class CreateRules < ActiveRecord::Migration
  def change
    create_table :rules do |t|
      t.string :name, null: false
      t.string :rule_method, null: false

      t.timestamps
    end
    add_index :rules, :rule_method
  end
end
