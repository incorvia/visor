class AddSpecGroupIdToSpec < ActiveRecord::Migration
  def change
    add_column :specs, :spec_group_id, :integer
    add_index :specs, :spec_group_id
  end
end
