class AddGalleryIdToImage < ActiveRecord::Migration
  def change
    add_column :images, :gallery_id, :integer
    add_index :images, :gallery_id
  end
end
