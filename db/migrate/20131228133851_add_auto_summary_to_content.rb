class AddAutoSummaryToContent < ActiveRecord::Migration
  def change
    add_column :contents, :custom_summary, :boolean, default: false
  end
end
