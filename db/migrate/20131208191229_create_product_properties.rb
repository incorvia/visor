class CreateProductProperties < ActiveRecord::Migration
  def change
    create_table :product_properties do |t|
      t.integer :property_id, null: false
      t.integer :product_id, null: false
      t.string :value, null: false

      t.timestamps
    end
    add_index :product_properties, :property_id
    add_index :product_properties, :product_id
    add_index :product_properties, [:product_id, :product_id], unique: true
  end
end
