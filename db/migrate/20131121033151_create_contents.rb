class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.string :title, null: false
      t.text :content, null: false
      t.string :type, null: false
      t.integer :product_id

      t.timestamps
    end
    add_index :contents, :type
    add_index :contents, :product_id
    add_index :contents, [:product_id, :type], unique: true
  end
end
