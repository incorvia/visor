class AddRankToProduct < ActiveRecord::Migration
  def change
    add_column :products, :rank, :integer
    add_column :products, :percentile, :integer
    add_index :products, :rank
    add_index :products, :percentile
  end
end
