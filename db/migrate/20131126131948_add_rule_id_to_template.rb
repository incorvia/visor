class AddRuleIdToTemplate < ActiveRecord::Migration
  def change
    add_column :templates, :rule_id, :integer
    add_index :templates, :rule_id
  end
end
