module Filters

  def ordinalize(input)
    input.ordinalize.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse
  end

  def delimit(input)
    ActionController::Base.helpers.number_with_delimiter(input)
  end

  def round(num)
    num.round(-1 * (num.to_s.split('.').first.to_s.size - 1))
  end

  def truncate(num)
    num.to_i
  end
end
