module ScoreDisplayable

  def display_score_value(base = 100)
    value = score_value / 100 * base
    value.round
  end

  def display_percentile
    "#{percentile.to_s}%"
  end
end
