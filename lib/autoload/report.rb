module Report
  def self.missing_spec_values!
    ::Product.all.find_each do |product|
      specs = product.specs

      specs.each do |spec|
        unless product.spec_values.where(spec_id: spec.id).count > 0
          puts "Product: #{product.name} is missing spec: #{spec.name} with id: #{spec.id}"
        end
      end
    end
  end

  def self.missing_properties!
    ::Product.all.find_each do |product|
      missing = product.missing_properties

      missing.each do |property|
        puts "Product: #{product.name} is missing property: #{property.name} with id: #{property.id}"
      end
    end
  end
end
