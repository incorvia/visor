namespace :report do
  desc "Print to screen products and missing spec values"
  task :missing_spec_values => :environment do
    Report.missing_spec_values!
  end

  desc "Print to screen products and missing properties"
  task :missing_properties => :environment do
    Report.missing_properties!
  end
end
