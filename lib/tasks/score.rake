namespace :score do
  desc "Create scores for all scoreable models"
  task :all => :environment do
    Rake::Task["score:specs"].invoke
    Rake::Task["score:specgroups"].invoke
    Rake::Task["score:products"].invoke
  end

  desc "Create scores for all products"
  task :specs => :environment do
    SpecCalculator.run!
  end

  desc "Create scores for all spec groups"
  task :specgroups => :environment do
    SpecGroupCalculator.run!
  end

  desc "Create scores for all specs"
  task :products => :environment do
    ProductCalculator.run!
  end
end
