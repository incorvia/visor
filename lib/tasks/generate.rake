namespace :generate do
  desc "Generate review for all products"
  task :reviews => :environment do
    ReviewGenerator.run!
  end
end

