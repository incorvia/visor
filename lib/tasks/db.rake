namespace :db do
  desc "Drops, creates, migrates, and seeds database"
  task :refresh => :environment do
    Rake::Task["db:drop"].invoke
    Rake::Task["db:create"].invoke
    Rake::Task["db:migrate"].invoke
    Rake::Task["db:seed"].invoke
    Rake::Task["score:all"].invoke
    Rake::Task["generate:reviews"].invoke
  end
end

namespace :db do
  namespace :test do
    desc "Drops and sets up test database using schema"
    task :refresh => :environment do
      Rake::Task["db:drop"].invoke
      Rake::Task["db:setup"].invoke
    end
  end
end

namespace :db do
  namespace :heroku do
    desc "migrates, seeds, scores, and reviews"
    task :refresh => :environment do
      Rake::Task["db:migrate"].invoke
      Rake::Task["db:seed"].invoke
      Rake::Task["score:all"].invoke
      Rake::Task["generate:reviews"].invoke
    end
  end
end
