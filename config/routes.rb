Visor::Application.routes.draw do
  get "reviews/show"
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  # Redirect root for now
  # get "/" => redirect("/reviews/google-nexus-5")
  get "/" => 'home#index'

  # Robots.txt
  get '/robots.txt' => 'home#robots'

  # Reviews
  resources :reviews, only: [:show]

  # QuestionForm
  resources :question_forms, only: [:create]
end
