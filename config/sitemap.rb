require 'rubygems'
require 'sitemap_generator'

# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = 'http://www.visor.io'
# pick a place safe to write the files
SitemapGenerator::Sitemap.public_path = 'tmp/'
# inform the map cross-linking where to find the other maps
SitemapGenerator::Sitemap.sitemaps_host = "http://#{configatron.amazon.s3.bucket}.s3.amazonaws.com/"
# pick a namespace within your bucket to organize your maps
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'
# store on S3 using Fog
SitemapGenerator::Sitemap.adapter = SitemapGenerator::S3Adapter.new({
  aws_access_key_id: configatron.amazon.s3.access_key_id,
  aws_secret_access_key: configatron.amazon.s3.secret_access_key,
  fog_directory: configatron.amazon.s3.bucket,
  fog_provider: 'AWS'
})

SitemapGenerator::Sitemap.create do
  Content.find_each do |content|
    add review_path(content), lastmod: content.updated_at
  end
end