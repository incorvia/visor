require 'dragonfly'

# Configure
Dragonfly.app.configure do
  plugin :imagemagick

  protect_from_dos_attacks true
  secret "2rpf6xnbtya7yojgzktxi"

  url_format '/media/:job/:name'   
  url_host configatron.dragonfly.url_host

  if Rails.env.test?
    datastore :file,
      root_path: Rails.root.join('public/system/dragonfly', Rails.env),
      server_root: Rails.root.join('public')
  else
    datastore :s3,
      bucket_name: configatron.amazon.s3.bucket,
      access_key_id: configatron.amazon.s3.access_key_id,
      secret_access_key: configatron.amazon.s3.secret_access_key
  end
end

# Logger
Dragonfly.logger = Rails.logger

# Mount as middleware
Rails.application.middleware.use Dragonfly::Middleware

# Add model functionality
if defined?(ActiveRecord::Base)
  ActiveRecord::Base.extend Dragonfly::Model
  ActiveRecord::Base.extend Dragonfly::Model::Validations
end
