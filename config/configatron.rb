# Amazon
configatron.amazon.tracking_id = 'visor0d-20'
configatron.amazon.s3.bucket = ENV['AMAZON_S3_BUCKET'] || "visor-#{Rails.env}"
configatron.amazon.s3.access_key_id = ENV['AMAZON_S3_ACCESS_KEY_ID']
configatron.amazon.s3.secret_access_key = ENV['AMAZON_S3_SECRET_ACCESS_KEY']
configatron.amazon.product_api.access_key_id = ENV['AMAZON_PRODUCT_ACCESS_KEY_ID']
configatron.amazon.product_api.secret_access_key = ENV['AMAZON_PRODUCT_SECRET_ACCESS_KEY']

# Dragonfly
configatron.dragonfly.url_host = ENV['DRAGONFLY_URL_HOST'] || 'http://localhost:5000'

# Rollbar
configatron.rollbar.access_token = ENV['ROLLBAR_ACCESS_TOKEN']
