ActiveAdmin.register Image do

  form do |f|
    f.inputs "Image Details" do
      f.input :product
      f.input :name
      f.input :image_url
      f.input :tag_list
      f.input :gallery_list
      f.input :location_list
      f.input :description
    end
    f.actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :product
      row 'remote_url' do |image|
        url = image.image.remote_url
        link_to url, url
      end
      row :tag_list
      row :gallery_list
      row :location_list
      row :description
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  index do
    column "Thumbnail" do |image|
      image_tag(image.image.thumb("75x75").url)
    end
    column :id
    column :name
    column :description
    column :created_at
    column :updated_at
    default_actions
  end

  permit_params do
    [:name, :product_id, :image_url, :image_uid, :width, :height, :description,
     :tag_list, :gallery_list, :location_list]
  end
end
