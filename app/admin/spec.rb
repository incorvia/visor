ActiveAdmin.register Spec do
  menu :parent => "Product"

  filter :name
  filter :spec_group
  filter :include
  filter :created_at
  filter :updated_at

  permit_params do
    [:name, :spec_group_id, :weight, :include, :notes]
  end

  controller do
    #TODO: Check if this is right implementation of new linking
    def resource
      if params[:action] == 'new'
        @resource = Spec.new(spec_group_id: params[:spec_group_id])
      else
        super
      end
    end
  end
end
