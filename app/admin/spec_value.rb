ActiveAdmin.register SpecValue do
  menu :parent => "Product"

  filter :product
  filter :spec

  action_item only: :show do
    link_to "New Spec Value", new_admin_spec_value_path
  end

  index do
    column :id
    column 'Product' do |spec_value|
      link_to spec_value.product.name, admin_product_path(spec_value.product)
    end
    column 'Spec' do |spec_value|
      link_to spec_value.spec.name, admin_spec_path(spec_value.spec)
    end
    column :value
    column :created_at
    column :updated_at
    default_actions
  end

  permit_params do
    [:product_id, :spec_id, :value]
  end

  controller do
    def scoped_collection
      SpecValue.includes(:spec, :product)
    end
  end
end
