ActiveAdmin.register CategoriesProperty do
  menu :parent => "Product"

  permit_params do
    [:property_id, :category_id]
  end
end
