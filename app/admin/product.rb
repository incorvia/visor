ActiveAdmin.register Product do
  menu :parent => "Product"

  filter :name
  filter :category
  filter :brand_id
  filter :asin
  filter :rank
  filter :percentile

  show do
    attributes_table do
      row :id
      row :name
      row :created_at
      row :updated_at
      row :display_score_value
      row :rank
      row :percentile
      row :asin
      row :brand
      row :price
    end

    product.spec_groups.each do |group|
      values = SpecValue.where(product_id: product.id, spec_id: group.specs.map(&:id)).includes(:score, :spec)

      panel group_title(group, product) do
        if values.count > 0
          table_for values do |spec_value|
            column "Spec Name", :display_name
            column :value
            column "Score", :display_score_value
            column :rank
            column "Percentile", :display_percentile
            column "Included?" do |sv|
              sv.spec.include
            end
            column "Actions" do |sv|
              action_links(sv, "spec_value")
            end
          end
        else
          "No Spec Values"
        end
      end
    end

    active_admin_comments
  end

  permit_params do
    [:name, :category_id, :asin, :brand_id]
  end
end
