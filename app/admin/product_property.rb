ActiveAdmin.register ProductProperty do
  menu :parent => "Product"

  permit_params do
    [:value, :product_id, :property_id]
  end
end
