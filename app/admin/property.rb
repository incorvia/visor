ActiveAdmin.register Property do
  menu :parent => "Product"

  show do
    attributes_table do
      row :id
      row :name
      row :created_at
      row :updated_at
    end

    panel "Categories" do
      table_for property.categories do
        column :id
        column :name
        column :created_at
        column :updated_at
      end
    end
    active_admin_comments
  end


  permit_params do
    [:name]
  end
end
