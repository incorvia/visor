ActiveAdmin.register Content do
  menu :parent => "Content"

  index do
    column :id
    column :title
    column :slug
    column :created_at
    column :updated_at
    default_actions
  end

  show do |content|
    attributes_table do
      row :id
      row :title
      row "slug" do
        link_to(content.slug, review_path(content), target: "_blank")
      end
      row :type
      row :product
      row :custom_summary
      row :summary
      row :created_at
      row :updated_at
    end

    panel "Review Content" do
      attributes_table_for content do
        row "Rendered Content" do
          content.content.html_safe
        end
        row :content
      end
    end

    active_admin_comments
  end

  controller do
    def find_resource
      scoped_collection.friendly.find(params[:id])
    end
  end

  permit_params do
    [:title, :slug]
  end
end
