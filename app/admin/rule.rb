ActiveAdmin.register Rule do
  menu :parent => "Content"

  permit_params do
    [:name, :rule_method]
  end
end
