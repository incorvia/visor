ActiveAdmin.register Category do
  menu :parent => "Product"

  action_item only: :show do
    link_to "New Spec Group", new_admin_spec_group_path(category_id: resource.id)
  end

  show do
    attributes_table do
      row :id
      row :name
      row :created_at
      row :updated_at
    end

    panel "Spec Groups" do
      table_for category.spec_groups do
        column "name" do |spec_group|
          link_to spec_group.name, admin_spec_group_path(spec_group)
        end
      end
    end

    panel "Properties" do
      table_for category.properties do
        column :id
        column :name
        column :created_at
        column :updated_at
      end
    end
    active_admin_comments
  end

  permit_params do
    [:name]
  end
end
