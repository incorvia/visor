ActiveAdmin.register Template do
  menu :parent => "Content"

  form do |f|
    f.inputs "Details" do
      f.input :name
      f.input :parent_id
      f.input :category
      f.input :rule
      f.input :rule_parameters
    end
    f.inputs "Content" do
      f.input :content
    end
    f.actions
  end

  permit_params do
   [:name, :content, :parent_id, :category_id, :rule_id, :rule_parameters]
  end
end
