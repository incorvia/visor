ActiveAdmin.register SpecGroup do
  menu :parent => "Product"

  action_item only: :show do
    link_to "New Spec", new_admin_spec_path(spec_group_id: resource.id)
  end

  index do
    column :id
    column :name
    column 'Category' do |spec_group|
      link_to spec_group.category.name, admin_category_path(spec_group.category)
    end
    column :weight
    column :created_at
    column :updated_at
    default_actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :category, admin_category_path(spec_group.category)
      row :created_at
      row :updated_at
    end

    panel "Specs" do
      table_for spec_group.specs do
        column "name" do |spec|
          link_to spec.name, admin_spec_path(spec)
        end
      end
    end
    active_admin_comments
  end

  permit_params do
    [:name, :category_id, :weight, :include]
  end

  controller do
    #TODO: Check if this is right implementation of new linking
    def resource
      if params[:action] == 'new'
        @resource = SpecGroup.new(category_id: params[:category_id])
      else
        super
      end
    end

    def scoped_collection
      SpecGroup.includes(:category)
    end
  end
end
