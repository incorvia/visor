class SpecScoreTag < Liquid::Tag

  def initialize(name, spec_name, tokens)
    @name = name
    @spec_name = spec_name.strip
  end

  def render(context)
    product = context["product"].product
    product.spec_score(@spec_name).display_score_value
  end
end

Liquid::Template.register_tag 'spec_score', SpecScoreTag
