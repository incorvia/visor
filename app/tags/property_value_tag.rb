class PropertyValueTag < Liquid::Tag

  def initialize(name, property_name, tokens)
    @name = name
    @property_name = property_name.strip
  end

  def render(context)
    product = context["product"].product
    product.property_value(@property_name)
  end
end

Liquid::Template.register_tag 'property_value', PropertyValueTag
