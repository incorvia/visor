class VincludeTag < Liquid::Tag

  def initialize(name, fetch_name, tokens)
    @name = name
    @fetch_name = fetch_name.strip
  end

  def render(context)
    # Get template / product from context
    template = context["template"].template
    product = context["product"].product

    # Fetch template
    new_template = template.child_by_rules(@fetch_name, product)

    # Build new context
    new_context = {
      'product' => ProductDrop.new(product),
      'template' => TemplateDrop.new(new_template),
      'top_product' => ProductDrop.new(product.top_product)
    }

    # Merge into context
    new_context['template'] = TemplateDrop.new(new_template)

    # Render
    t = Liquid::Template.parse(new_template.content)
    t.render(new_context)
  end
end

Liquid::Template.register_tag 'vinclude', VincludeTag
