# == Schema Information
#
# Table name: spec_groups
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  category_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#  include     :boolean
#  weight      :integer
#

class SpecGroup < ActiveRecord::Base
  belongs_to :category
  has_many :specs
  has_many :products, through: :category
  has_many :scores, as: :scoreable

  def self.includeable
    where(include: true)
  end

  def score(product)
    Score.where(product_id: product.id, scoreable_type: 'SpecGroup', scoreable_id: self.id).first
  end

  def included_specs
    specs.where(include: true)
  end

  def to_score(product)
    specs = self.specs.includeable.includes(:scores)
    product.score_scoreables(specs)
  end
end
