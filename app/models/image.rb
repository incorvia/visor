class Image < ActiveRecord::Base
  belongs_to :product

  dragonfly_accessor :image do
    default Rails.root.join('public', 'images', 'default.png')
  end

  acts_as_taggable
  acts_as_taggable_on :galleries
  acts_as_taggable_on :locations

  validates_presence_of :name

  def self.default_image
    Image.new(name: 'Default Product Image')
  end

  def image_name
    self.image_uid
  end
end
