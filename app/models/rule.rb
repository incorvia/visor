# == Schema Information
#
# Table name: rules
#
#  id          :integer          not null, primary key
#  name        :string(255)      not null
#  rule_method :string(255)      not null
#  created_at  :datetime
#  updated_at  :datetime
#

class Rule < ActiveRecord::Base
  has_many :templates

  #TODO refactor common code
  def self.product_percentile(product, args)
    args.map!(&:to_i)
    min, max = args
    percentile = product.percentile
    max >= percentile && percentile >= min
  end

  def self.spec_value_in_range(product, args)
    spec = args.shift
    args.map!(&:to_f)
    min, max = args
    value = product.spec_value(spec).value.to_f
    max >= value && value >= min
  end

  def self.spec_percentile_in_range(product, args)
    generic_in_range(product, "spec_score", args)
  end

  def self.spec_group_percentile_in_range(product, args)
    generic_in_range(product, "spec_group_score", args)
  end

  def self.generic_in_range(product, method, args)
    spec = args.shift
    args.map!(&:to_f)
    min, max = args
    value = product.send("#{method}", spec).percentile.to_f
    max >= value && value >= min
  end

  def self.match_property_value(product, args)
    property_string, value = args
    product.property_value(property_string) == value
  end
end
