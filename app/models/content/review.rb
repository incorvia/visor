# == Schema Information
#
# Table name: contents
#
#  id         :integer          not null, primary key
#  title      :string(255)      not null
#  content    :text             not null
#  type       :string(255)      not null
#  product_id :integer
#  created_at :datetime
#  updated_at :datetime
#  slug       :string(255)      not null
#

class Content
  class Review < Content
    belongs_to :product

    def self.fuzzy_find(id)
      includes(:product).friendly.find(id)
    end

    def self.default_title(name)
      "#{name} Review"
    end
  end
end
