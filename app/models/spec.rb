# == Schema Information
#
# Table name: specs
#
#  id            :integer          not null, primary key
#  name          :string(255)      not null
#  created_at    :datetime
#  updated_at    :datetime
#  spec_group_id :integer
#  include       :boolean
#  weight        :integer
#

class Spec < ActiveRecord::Base
  has_many :spec_values
  has_many :scores, as: :scoreable
  belongs_to :spec_group

  validates_presence_of :name
  validates_uniqueness_of :name, scope: :spec_group_id

  def self.includeable
    where(include: true)
  end
end
