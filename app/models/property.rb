class Property < ActiveRecord::Base
  has_and_belongs_to_many :categories
  has_many :product_properties

  accepts_nested_attributes_for :categories, allow_destroy: true

  validates_presence_of :name
  validates_uniqueness_of :name

  before_save :underscore_name

  private

  def underscore_name
    if self.name
      self.name = self.name.parameterize.underscore
    end
  end
end
