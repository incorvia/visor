# == Schema Information
#
# Table name: spec_values
#
#  id         :integer          not null, primary key
#  product_id :integer          not null
#  spec_id    :integer          not null
#  created_at :datetime
#  updated_at :datetime
#  value      :decimal(, )      not null
#

class SpecValue < ActiveRecord::Base
  has_one :score
  belongs_to :product
  belongs_to :spec

  delegate :score_value, :rank, :percentile, :display_score_value,
    :display_percentile, to: :score, allow_nil: true

  validates_uniqueness_of :product_id, scope: :spec_id

  def display_name
    spec.name
  end

  def to_score(max)
    Score.calculate_score(max, self.value.to_f)
  end
end
