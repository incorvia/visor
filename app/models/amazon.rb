class Amazon
  def initialize
    @amz = Vacuum.new
    @amz.configure(
      aws_access_key_id:     configatron.amazon.product_api.access_key_id,
      aws_secret_access_key: configatron.amazon.product_api.secret_access_key,
      associate_tag:         configatron.amazon.tracking_id
    )
  end

  def item_lookup(asin)
    Hashie::Mash.new(@amz.item_lookup(
      'ItemId' => asin,
      'IdType' => 'ASIN',
      'ResponseGroup' => 'OfferSummary',
      'MerchantId' => 'All'
    ).to_h)
  end

  def price_lookup(asin)
    price = nil
    unless Rails.env.test?
      response = item_lookup(asin)
      price = response.ItemLookupResponse.Items.Item.OfferSummary.LowestNewPrice.Amount.to_i
    end
    return price
  end
end
