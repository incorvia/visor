# == Schema Information
#
# Table name: templates
#
#  id              :integer          not null, primary key
#  name            :string(255)      not null
#  content         :text             not null
#  created_at      :datetime
#  updated_at      :datetime
#  ancestry        :string(255)
#  category_id     :integer
#  rule_id         :integer
#  rule_parameters :string(255)
#

class Template < ActiveRecord::Base
  has_ancestry

  belongs_to :category
  belongs_to :rule

  before_save :underscore_name

  def render(context={})
    context.merge( "template" => TemplateDrop.new(self) )
    template = Liquid::Template.parse(self.content)
    template.render(context)
  end

  def children_by_name(name)
    self.children.where(name: name).includes(:rule)
  end

  def child_by_rules(name, product)
    templates = self.children_by_name(name)
    selected = product.select_templates_by_rules(templates)
    selected.sample(1).first
  end

  private

  def underscore_name
    self.name = self.name.parameterize.underscore
  end
end
