# == Schema Information
#
# Table name: categories
#
#  id            :integer          not null, primary key
#  name          :string(255)      not null
#  created_at    :datetime
#  updated_at    :datetime
#  active_months :integer          not null
#

class Category < ActiveRecord::Base
  has_many :spec_groups
  has_many :specs, through: :spec_groups
  has_many :products
  has_and_belongs_to_many :properties
  has_one :template

  accepts_nested_attributes_for :properties, allow_destroy: true

  def top_product
    products.where(rank: 1).first
  end
end
