# == Schema Information
#
# Table name: products
#
#  id           :integer          not null, primary key
#  name         :string(255)      not null
#  created_at   :datetime
#  updated_at   :datetime
#  category_id  :integer          not null
#  review_title :string(255)
#

class Product < ActiveRecord::Base
  include ScoreDisplayable

  has_many :spec_values
  has_many :spec_groups, through: :category
  has_many :specs, through: :spec_groups
  has_many :scores
  has_many :images
  has_many :product_properties
  has_many :required_properties, source: :properties, through: :category
  has_many :properties, through: :product_properties
  has_one :product_score, as: :scoreable, class_name: 'Score'
  has_one :review, class_name: 'Content::Review'
  has_one :template, through: :category
  belongs_to :category
  belongs_to :brand

  validates_presence_of :name
  validates_presence_of :brand_id

  before_save :set_amazon_price

  delegate :top_product, to: :category

  def full_name
    "#{brand_name} #{name}"
  end

  def brand_name
    brand.try(:name)
  end

  def spec(name)
    self.specs.select { |x| x.name == name }.first
  end

  def spec_score(name)
    spec = self.spec(name)
    return nil unless spec
    self.scores.where(scoreable_type: 'Spec', scoreable_id: spec.id).first
  end

  def spec_group(name)
    self.spec_groups.select { |x| x.name == name }.first
  end

  def spec_group_score(name)
    group = spec_group(name)
    return nil unless group
    self.scores.where(scoreable_type: 'SpecGroup', scoreable_id: group.id).first
  end

  def spec_value(name)
    spec = self.spec(name)
    return nil unless spec
    self.spec_values.where(spec_id: spec.id).first
  end

  def property_value(property_string)
    property = Property.where(name: property_string).first
    pp = self.product_properties.where(property_id: property.id).first
    pp.value
  end

  def to_score
    spec_groups = self.spec_groups.includeable.includes(:scores)
    self.score_scoreables(spec_groups)
  end

  def group_scores
    self.scores.group_scores
  end

  def score_scoreables(scoreables)
    total_score, total_weight = [0,0]

    scoreables.map do |scoreable|
      scores = self.scores.where(
        scoreable_id: scoreable.id,
        scoreable_type: scoreable.class.to_s,
        product_id: self.id
      ).to_a

      scores.each do |score|
        score_value = score ? score.score_value.to_f : 0
        weight = scoreable.weight || 1
        total_score += score_value * weight
        total_weight += weight
      end
    end

    total_weight.zero? ? 0 : total_score/total_weight
  end

  def category_specs
    Spec.where(spec_group_id: spec_groups.map(&:id))
  end

  def related_products(count = 3)
    Product.where('id not in (?)', [self.id]).includes(:review).order("RANDOM()").limit(count)
  end

  def missing_properties
    (self.required_properties - self.properties).to_a
  end

  def vanity
    @vanity ||= begin
      self.images.tagged_with("vanity", on: :locations).sample(1).first || Image.default_image
    end
  end

  def price
    amazon_price / 100 if amazon_price
  end

  def amazon_link
    "http://www.amazon.com/dp/#{self.asin}/?tag=#{configatron.amazon.tracking_id}"
  end

  def set_amazon_price
    if update_amazon_price?
      self.amazon_price = Amazon.new.price_lookup(self.asin)
      self.amazon_price_updated_at = Time.now
    end
  end

  def update_amazon_price?
    !amazon_price_updated_at || (amazon_price_updated_at < 48.hours.ago)
  end

  def render_review
    context = {
      'product' => ProductDrop.new(self),
      'template' => TemplateDrop.new(self.template),
      'top_product' => ProductDrop.new(self.top_product)
    }
    self.template.render(context)
  end

  def review_or_build
    self.review || self.build_review
  end

  def select_templates_by_rules(templates)
    eligible = []
    templates.each do |template|
      if template.rule_parameters
        args = template.rule_parameters.split(',')
      end

      if !template.rule || Rule.send(template.rule.rule_method, self, args)
        eligible << template
      end
    end
    return eligible
  end

  def gallery(tag, return_default =  nil)
    images = self.images.tagged_with([tag], on: :galleries).
      partition { |x| x.tag_list.include?('gallery_key') }.flatten
    if images.empty? && return_default
      images = [Image.default_image]
    end
    return images
  end
end
