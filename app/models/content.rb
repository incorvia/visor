# == Schema Information
#
# Table name: contents
#
#  id         :integer          not null, primary key
#  title      :string(255)      not null
#  content    :text             not null
#  type       :string(255)      not null
#  product_id :integer
#  created_at :datetime
#  updated_at :datetime
#  slug       :string(255)      not null
#

class Content < ActiveRecord::Base
  include ActionView::Helpers::SanitizeHelper

  belongs_to :product

  include FriendlyId

  before_save :update_summary

  friendly_id :slug_method, use: [:slugged]

  def slug_method
    product.try(:full_name) || self.title
  end

  private

  def update_summary
    unless self.custom_summary
      matches = self.content.match(/<p\b[^>]*>(.*?)<\/p>/)
      self.summary = strip_tags(matches[0]) if matches
    end
  end
end
