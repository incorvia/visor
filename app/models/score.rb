# == Schema Information
#
# Table name: scores
#
#  id             :integer          not null, primary key
#  score_value    :decimal(, )      not null
#  rank           :integer
#  percentile     :integer
#  scoreable_type :string(255)      not null
#  scoreable_id   :integer          not null
#  spec_value_id  :integer
#  product_id     :integer          not null
#  created_at     :datetime
#  updated_at     :datetime
#

class Score < ActiveRecord::Base
  include ScoreDisplayable

  belongs_to :product
  belongs_to :spec_value
  belongs_to :scoreable, polymorphic: true

  validates_presence_of :spec_value_id, if: Proc.new { |score| score.scoreable_type == 'Spec' }
  validates_uniqueness_of :product_id, scope: [:scoreable_id, :scoreable_type]

  delegate :name, to: :scoreable

  after_save :set_score_on_product

  def self.group_scores
    where(scoreable_type: 'SpecGroup')
  end

  def self.calculate_score(max, value)
    ((value.to_f / max.to_f ) * 100).round(2)
  end

  def self.calculate_percentile(count, rank)
    ((count - rank) * 100)/count
  end

  def update_ranking(rank, percentile)
    self.update_attributes(
      percentile: percentile,
      rank: rank
    )
  end

  def set_score_on_product
    if self.scoreable_type == 'Product'
      self.scoreable.update_attributes(
        rank: self.rank,
        percentile: self.percentile,
        score_value: self.score_value
      )
    end
  end
end
