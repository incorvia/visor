class ReviewGenerator

  def self.run!
    Product.all.find_each do |product|
      generator = ReviewGenerator.new(product)
      generator.generate!
    end
  end

  def initialize(product)
    @product = product
  end

  def generate!
    review = @product.review_or_build
    review.content = @product.render_review
    review.title = ::Content::Review.default_title(@product.name) if !review.title
    review.save
  end
end
