# SUIT Slider

[![Build Status](https://secure.travis-ci.org/user/suit-slider.png?branch=master)](http://travis-ci.org/user/suit-slider)

A SUIT component for...

Read more about [SUIT's design principles](https://github.com/suitcss/suit/).

## Installation

* [Bower](http://bower.io/): `bower install --save suit-slider`
* [Component(1)](http://component.io/): `component install user/suit-slider`
* Download: [zip](https://github.com/user/suit-slider/zipball/master)
* Git: `git clone https://github.com/user/suit-slider.git`


## Available classes

* `Slider` - The core component class

## Usage

```html
<div class="Slider">

</div>
```

## Testing

Install [Node](http://nodejs.org) (comes with npm). It's recommended that you
also globally install [Component(1)](http://component.io): `npm install -g
component`.

From the repo root, install the project's development dependencies:

```
make
```

To run the CSS Lint tests and build the front-end development bundle:

```
make test
```

Basic visual tests are in `test.html`.

## Browser support

* Google Chrome (latest)
* Opera (latest)
* Firefox 4+
* Safari 5+
* Internet Explorer 8+
