# SUIT Social

[![Build Status](https://secure.travis-ci.org/user/suit-social.png?branch=master)](http://travis-ci.org/user/suit-social)

A SUIT component for...

Read more about [SUIT's design principles](https://github.com/suitcss/suit/).

## Installation

* [Bower](http://bower.io/): `bower install --save suit-social`
* [Component(1)](http://component.io/): `component install user/suit-social`
* Download: [zip](https://github.com/user/suit-social/zipball/master)
* Git: `git clone https://github.com/user/suit-social.git`


## Available classes

* `Social` - The core component class

## Usage

```html
<div class="Social">

</div>
```

## Testing

Install [Node](http://nodejs.org) (comes with npm). It's recommended that you
also globally install [Component(1)](http://component.io): `npm install -g
component`.

From the repo root, install the project's development dependencies:

```
make
```

To run the CSS Lint tests and build the front-end development bundle:

```
make test
```

Basic visual tests are in `test.html`.

## Browser support

* Google Chrome (latest)
* Opera (latest)
* Firefox 4+
* Safari 5+
* Internet Explorer 8+
