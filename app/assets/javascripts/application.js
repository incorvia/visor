// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require modernizr/modernizr
//= require jquery/jquery
//= require skeljs/skel.min
//= require skeljs/skel-panels.min
//= require magnific-popup/dist/jquery.magnific-popup
//= require nav
//= require jquery.glide
//= require sliders
//= require social-count/dist/socialcount
//= require search
//= require jquery-placeholder/jquery.placeholder
//= require jquery.mambo.min
//= require visor