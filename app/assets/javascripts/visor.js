// Sitewide JS

// SkelJS
var skelConfig = {

  // Setup SkelJS
    skelJS: {
      prefix: '/assets/devices/visor',
      resetCSS: false,
      boxModel: 'border',
      useOrientation: true,
      breakpoints: {
        'widest': { range: '1881-', hasStyleSheet: false, containers: 1400, grid: { gutters: 70 } },
        'wide': { range: '961-1880', containers: 1200, grid: { gutters: 40 } },
        'normal': { range: '961-1620', hasStyleSheet: false, containers: 960, grid: { gutters: 40 } },
        'narrow': { range: '961-1320', hasStyleSheet: false, containers: 'fluid', grid: { gutters: 30 } },
        'narrower': { range: '-960', containers: 'fluid', grid: { gutters: 30 } },
        'mobile': { range: '-640', containers: 'fluid', lockViewport: true, grid: { gutters: 30, collapse: true } }
      }
    },

  // skelJS Plugins
    skelJSPlugins: {
      panels: {
        panels: {searchPanel: {
            breakpoints: 'mobile',
            position: 'right',
            size: '80%',
            html: '<div data-action="moveElement" data-args="vsrSearchForm"></div></div>',
            swipeToClose: true
          },
          infoPanel: {
            // breakpoints: 'narrower',
            position: 'left',
            size: '275px',
            swipeToClose: true
          }
        },
        overlays: {
          mobileHeader: {
            position: 'top-left',
            width: '100%'
          }
        }
      }
    }

};

// Initialize skelJS
skel.init(skelConfig.skelJS, skelConfig.skelJSPlugins);

// Vars
var $doc = $(document);

// App Object
var visor = {
  init: function() {
    this.analytics.init();
    this.common.init();
    this.reviews.init();
  },

  analytics: {
    init: function() {
      // Vars
      var $trackingItems  = $('[data-track="true"]'),
      trackingEvent   = {},
      eventData       = {},
      category        = '',
      action          = '',
      location        = '';

      // Events
      $doc.on('track.event', function(e, data) {
          // Vars
          eventData.category = eventData.category || data.category; // Required.
          eventData.action   = eventData.action   || data.action; // Required.
          eventData.label    = eventData.label    || data.label;
          e.preventDefault();

          // Track Event
          visor.analytics.trackEvent(eventData);
          // Reset The Event Data Object
          eventData = {};
        });

      $trackingItems.on('click', function(e) {
        eventData.category = this.getAttribute('data-category') || '';
        eventData.action   = this.getAttribute('data-action') || '';
        eventData.label    = this.getAttribute('data-location') || '';
        $doc.trigger('track.event', eventData);
      });


    },
    trackEvent: function(e) {
      // Track the search term in Google Analytics
      ga('send', {
          'hitType': 'event',                 // Required.
          'eventCategory': e.category,        // Required.
          'eventAction': e.action,            // Required.
          'eventLabel': e.label
        });
    }
  },

  common: {
    init: function() {
      // Remove No-JS class
      $('body').removeClass('no-js');

      // Init Popus
      $('.open-popup-link').magnificPopup({
        type:'inline',
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
      });

      // Init Placeholder Support
      $('input, textarea').placeholder();
    }
  },

  reviews: {
    init: function() {
      // Init Bar Charts
      var valueData = $('.valueData'),
      value = 0;
      for (var i = valueData.length - 1; i >= 0; i--) {
        valueData[i].className += ' valueData--' + valueData[i].getAttribute('data-value');
      };

      // Init Mambo Plugin
      function isCanvasSupported(){
        var elem = document.createElement('canvas');
        return !!(elem.getContext && elem.getContext('2d'));
      }

      if (isCanvasSupported()) {
        var $review_meta_score  = $("#review_meta_score"),
            reviewScoreValue    = $review_meta_score.data('value'),
            reviewScoreColour   = '#0aa699';

        reviewScoreColour = reviewScoreValue >= 50 ? reviewScoreColour : '#f35958';

        $review_meta_score.mambo({
          percentage: reviewScoreValue,
          displayValue: true,
          circleColor: '#fff',
          labelColor: reviewScoreColour,
          ringStyle: "full",
          ringBackground: "#e5e5e5",
          ringColor: reviewScoreColour,
          drawShadow: false
        });
      } else {
        $('.vsr-ReviewMeta-score').addClass('visible');
      }
    }
  }

};

$(function() {
  visor.init();
});
