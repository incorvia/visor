$(function() {

    // Vars
    var $doc                    = $(document),
        $vsrSearchForm          = $('#vsrSearchForm'),
        $vsrSearchInput         = $('#vsrSearchInput', $vsrSearchForm),
        $vsrSearchSubmit        = $('#vsrSearchSubmit', $vsrSearchForm),
        $vsrSearchResultsPanel  = $('#vsrSearchResultsPanel', $vsrSearchForm),
        $searchTermPlaceholders = $('.searchTermValue'),
        searchActive            = false,
        searchInputValue        = '',
        trackEventData          = {};

    // Events
    $vsrSearchForm.on('submit', function(e) {
        e.preventDefault();
    }).on('open', function(e) {
        $vsrSearchResultsPanel.addClass('is-active');
        searchActive = true;
    }).on('close', function(e) {
        $vsrSearchResultsPanel.removeClass('is-active');
        searchActive = false;
    });

    $vsrSearchInput.on('change', function(e) {
        e.preventDefault();

        if ($(this).val() === '') {return false};

        searchInputValue = $(this).val();

        $searchTermPlaceholders.text(searchInputValue);

        // Track the search term in Google Analytics
        trackEventData.category = this.getAttribute('data-category');
        trackEventData.action   = searchInputValue;
        trackEventData.label    = this.getAttribute('data-location');

        $doc.trigger('track.event', trackEventData);

        $vsrSearchForm.trigger('open');

    }).on('focusin', $vsrSearchInput, function(e) {
        if ($vsrSearchInput.val() != '' && !searchActive) {
            $vsrSearchForm.trigger('open');
        }
    });

   $doc.on('click', function(e) {
        if (searchActive) {
            if ($(e.target).parents('form.vsr-Search').length) {
                return false;
            } else {
                $vsrSearchForm.trigger('close');
            }
        }
    });
});