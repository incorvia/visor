$(function() {
    // Variables
    var $doc = $(document),
        $navHolder                  = $('#navHolder', $doc),
        $navContentContainer        = $('#navContentContainer', $doc),
        $contentContainer           = $('.vsr-Content--main', $doc),
        $navContentItems            = $navContentContainer.find('.vsr-Nav-flyout'),
        $navContentContainerClose   = $('#navContentContainerClose', $navContentContainer),
        $navItems                   = $('#navItems', $doc)
        $navToggleSmalldevice       = $('#navToggleSmalldevice', $doc),
        $questionForm               = $('#questionForm', $doc),
        navItemTarget               = false,
        navActive                   = false,
        navActiveSD                 = false,
        contentId                   = '',
        questionFormAnswers         = {};

    // Events
    $navItems.on('open.nav', function(e, target) {
        contentId = target.currentTarget.getAttribute('data-target');
        $navItems.find('[data-behavior~=open]').removeClass('is-open');
        $(target.currentTarget).addClass('is-open');
        $navContentItems.removeClass('is-active');
        $navContentContainer.addClass('is-active').find('#'+contentId).addClass('is-active');
        navActive = true;
    }).on('close.nav', function(e) {
        $navContentContainer.removeClass('is-active');
        $navContentItems.removeClass('is-active');
        $navItems.find('[data-behavior~=open]').removeClass('is-open');
        navActive = false;
    }).on('click', '[data-behavior~=open]', function(e) {
        e.preventDefault();
        navItemTarget = $(e.currentTarget).hasClass('is-open');
        if (navItemTarget) {
            $navItems.trigger('close.nav', e);
        } else {
            $navItems.trigger('open.nav', e);
        }
    });

    $doc.on('click', function(e) {
        if (navActive) {

            // If the click is on a submit button then return true
            if (e.target.type === 'submit') {
                return true;
            }

            // If the click is on the nav flyout or one of its children, return false
            if ($(e.target).parents('.nav-target').length) {
                return false;
            }

            // If the click is not one of the navitems then close the nav
            navItemTarget = $(e.target).hasClass('nav-target');

            if (!navItemTarget) {
                $navItems.trigger('close.nav');
            }
        }
    });

    $questionForm.on('submit', function(e) {
        e.preventDefault();

        var email = document.getElementById('question_form_email');
        if (checkEmail(email)) {

            $(this).find('.Form-input').each(function(index) {
                // Send this through
                questionFormAnswers[this.name] = this.value;
            });

            var authenticity_token = $(this).find('#authenticity_token').val();

            questionFormAnswers.authenticity_token = authenticity_token;

            $.ajax({
                url: "/question_forms",
                data: questionFormAnswers,
                type: 'POST'
            }).done(function (data) {
                alert("Thanks for the question. We'll be in touch soon.");
                $navItems.trigger('close.nav');
                resetForm($questionForm);
            }).fail(function( data ) {
                console.log(data);
            });
        }
    });

    $navContentContainerClose.on('click', function(e) {
         $navItems.trigger('close.nav');
    });

    // Small Devices
    $navToggleSmalldevice.on('click', function(e) {
        e.preventDefault();
        if ($navContentContainer.hasClass('is-active')) {
            $navItems.trigger('close.nav', e);
            $navHolder.removeClass('is-active');
            $contentContainer.removeClass('u-fade-2');
            return false;
        }
        navActiveSD = $navHolder.hasClass('is-active');
        if (navActiveSD) {
            $navHolder.removeClass('is-active');
            $contentContainer.removeClass('u-fade-2');
        } else {
            $navHolder.addClass('is-active');
            $contentContainer.addClass('u-fade-2');
        }
    });

    // Functions
    function checkEmail(email) {

        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (!filter.test(email.value)) {
            alert('Please provide a valid email address');
            email.focus();
            return false;
        } else {
            return true;
        }
    }

    function resetForm(form) {
        form.find('input:text, select, textarea').val('');
    }

});