module ReviewsHelper

  def group_scores(product)
    @product.group_scores.sort_by { |s| s.name }
  end

  def show_gallery?(gallery)
    gallery.count > 0
  end
end
