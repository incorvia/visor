module ActiveAdmin::ApplicationHelper

  def action_links(obj, url_name)
    links = ''.html_safe
    links += link_to 'View', self.send("admin_#{url_name}_path", obj)
    links += ' '
    links += link_to 'Edit', self.send("edit_admin_#{url_name}_path", obj)
    links += ' '
    links += link_to("Delete", self.send("admin_#{url_name}_path", obj), method: :delete, class: 'member_link delete_link', "data-confirm" => "Are you sure you want to delete this?")
    links
  end
end
