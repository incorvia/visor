module ActiveAdmin::ProductHelper

  def group_title(group, product)
    score = group.score(product)
    if score
      "#{group.name}: (score: #{score.display_score_value}, rank: #{score.rank}, percentile: #{score.percentile})"
    else
      group.name
    end
  end
end
