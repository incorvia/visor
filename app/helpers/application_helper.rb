module ApplicationHelper

  def title_tag(content = nil)
    if content && content.title
      "#{content.title} | Visor"
    else
      "Visor"
    end
  end

  def description_tag(content = nil)
    if content && content.summary
      content.summary
    else
      "Visor was founded in 2014 to provide intelligent and always up-to-date product reviews that stay current as the market for products changes."
    end
  end

  def product_price(product)
    number_to_currency(product.price)
  end
end
