class QuestionForm < MailForm::Base
  attributes :name,  :validate => true
  attributes :email, :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attributes :question

  def headers
    {
      subject: "Question from: #{name}",
      to: "dev@visor.io",
      from: %("#{name}" <#{email}>)
    }
  end
end
