class ProductDrop < Liquid::Drop
  delegate :name, :full_name, :rank, :percentile, :display_type,
    to: :@product

  def initialize(product)
    @product = product
    @brand_name = @product.brand_name
  end

  def product
    @product
  end

  def better_products
    @product.rank - 1
  end

  def brand_name
    @brand_name
  end

  def count
    @count ||= Product.count
  end
end
