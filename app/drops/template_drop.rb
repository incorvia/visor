class TemplateDrop < Liquid::Drop

  def initialize(template)
    @template = template
  end

  def template
    @template
  end
end
