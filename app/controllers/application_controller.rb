class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :set_access
  before_filter :authenticate if Rails.env.staging?

  def set_access
    response.headers["Access-Control-Allow-Origin"] = "*"
  end

  protected

  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      admin = AdminUser.find_by_email(username)
      admin && admin.valid_password?(password)
    end
  end
end
