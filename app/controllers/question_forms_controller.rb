class QuestionFormsController < ApplicationController
  def create
    @form = QuestionForm.new(params[:question_form])
    @form.deliver
    head :ok
  end
end
