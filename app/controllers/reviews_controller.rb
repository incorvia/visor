class ReviewsController < ApplicationController
  def show
    @review = Content::Review.fuzzy_find(params[:id])
    @product = @review.product
    @sidebar_gallery = @product.gallery('right_sidebar_1', true)
    @related = @product.related_products
    @question_form = QuestionForm.new
    @search_products = @product.related_products(4)
  end
end
