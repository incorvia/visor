class SpecCalculator

  def self.run!
    self.generate_scores!
    self.generate_rankings!
  end

  def self.generate_rankings!
    ::Spec.all.find_each do |spec|
      scores = Score.
        select("*, rank() OVER (ORDER BY score_value DESC)").
        where(scoreable_type: 'Spec', scoreable_id: spec.id)
      count = scores.count

      scores.each do |score|
        score.rank_will_change!
        score.percentile = ::Score.calculate_percentile(count, score.rank - 1)
        score.save!
      end
    end
  end

  def self.generate_scores!
    ::Spec.all.find_each do |spec|
      spec_values = spec.spec_values.order(value: :desc)
      max = spec_values.first.try(:value).try(:to_f) || 0

      spec_values.each do |sv|
        score = sv.score || sv.build_score
        score.score_value = sv.to_score(max)
        score.scoreable_type = 'Spec'
        score.scoreable_id = spec.id
        score.product_id = sv.product_id
        score.spec_value_id = sv.id
        score.save
      end
    end
  end
end
