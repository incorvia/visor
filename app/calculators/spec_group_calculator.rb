class SpecGroupCalculator

  def self.run!
    self.generate_scores!
    self.generate_rankings!
  end

  def self.generate_rankings!
    SpecGroup.find_each do |spec_group|
      scores = Score.
        select("*, rank() OVER (ORDER BY score_value DESC)").
        where(scoreable_type: 'SpecGroup', scoreable_id: spec_group.id)
      count = scores.count

      scores.each do |score|
        score.rank_will_change!
        score.percentile = ::Score.calculate_percentile(count, score.rank - 1)
        score.save!
      end
    end
  end

  def self.generate_scores!
    ::SpecGroup.all.find_each do |spec_group|
      spec_group.products.each do |product|
        score = ::Score.where(
          product_id: product.id,
          scoreable_id: spec_group.id,
          scoreable_type: 'SpecGroup'
        ).first || ::Score.new

        score.product_id = product.id
        score.scoreable_type = 'SpecGroup'
        score.scoreable_id = spec_group.id
        score.score_value = spec_group.to_score(product)
        score.save
      end
    end
  end
end
