source 'https://rubygems.org'

# Ruby
ruby '2.0.0'

# Rails
gem 'rails', '~> 4.0.0'

# Database
gem 'pg'

# Assets
gem 'sass-rails', '~> 4.0.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'

# Config
gem 'foreman'

# Misc
gem 'activeadmin', github: 'gregbell/active_admin'
gem 'squeel'
gem 'liquid'
gem 'ancestry'
gem 'friendly_id', '> 5.0.0'
gem 'ruby-progressbar'
gem 'configatron'
gem 'acts-as-taggable-on'
gem 'dragonfly'
gem 'dragonfly-s3_data_store'
gem 'unf' # remove unf warning
gem 'vacuum'
gem 'hashie'
gem 'mail_form'
gem 'sitemap_generator'

# Heroku Addons
gem 'rollbar'

# Frontend
# gem 'bower-rails', '~> 0.5.0'
gem 'bower-rails', github: '42dev/bower-rails'
gem 'autoprefixer-rails'
gem 'font-awesome-sass'
gem "compass-rails", "~> 2.0.alpha.0"

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :production do
  gem 'rails_12factor'
  gem 'fog'
end

group :development, :test do
  gem 'pry', '0.9.12.2'
  gem 'pry-nav'
  gem 'pry-rescue'
  gem 'minitest-rails'
  gem 'factory_girl_rails'
  gem 'guard-minitest'
  gem 'railroady'
  gem 'turn'
  gem 'rails-footnotes'
  gem 'database_cleaner'
  gem 'annotate', '>=2.5.0'
  gem 'mocha', require: false
  gem 'simplecov'
  gem 'gitstats-ruby'
  gem 'letter_opener'
end
